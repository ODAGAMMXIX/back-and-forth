package splitVersion; //DIVIDIR EM CLASSES DIFERENTES;
/*abstract*/
public class Banks { // Why "abstract"?
	// Java program to demonstrate implementation of 
	// Strategy Pattern 

	// Abstract as you must have a specific fighter 
	//abstract class Fighter 
	//{ 
		BalanceDisplay balanceDisplay; //KickBehavior kickBehavior; 
		CashWithdrawal cashWithdrawal; // JumpBehavior jumpBehavior; 

/*		public Banks(BalanceDisplay balanceDisplay,  //KickBehavior kickBehavior, 
					    CashWithdrawal cashWithdrawal) //JumpBehavior jumpBehavior) 
		{			
			this.cashWithdrawal = cashWithdrawal;
			this.balanceDisplay = balanceDisplay;			
		}*/ 
		public void authentication() 
		{ 
			System.out.println("Default Authentication"); 
		} 
		public void display() //kick() 
		{ 
			 /* delegate to kick behavior*/ 
//			balanceDisplay.display; //kickBehavior.kick(); 
		} 
		public void withdraw() 
		{ 
			// delegate to jump behavior 
			cashWithdrawal.withdraw(); //jumpBehavior.jump(); 
		} 
		public void beep4card() 
		{ 
			System.out.println("Default beep4card Reminder"); 
		} 
		public void setBalanceDisplay(BalanceDisplay balanceDisplay) 
		{ 
			this.balanceDisplay = balanceDisplay; // OVERRIDING HERE?
		} 
		public void setcashWithdrawal(CashWithdrawal cashWithdrawal) 
		{ 
			this.cashWithdrawal = cashWithdrawal; 
		} 
		public void callBalanceDisplay() {
			balanceDisplay.display();
		}
		
		//public abstract void banktelling(); //display(); 
	}

	// Driver class 
	