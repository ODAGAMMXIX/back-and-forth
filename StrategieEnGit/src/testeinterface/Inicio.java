package testeinterface;

import java.util.ArrayList;

public class Inicio {
	public static void main(String[] args) {
		Pessoa p = new Pessoa("Jose", 33, 'm');
		Pessoa p1 = new Pessoa("Maria", 22, 'f');
		Pessoa p2 = new Pessoa("Ana", 27, 'f');
		
		ArrayList<Pessoa> listaPessoas = new ArrayList<Pessoa>();
		listaPessoas.add(p);
		listaPessoas.add(p1);
		listaPessoas.add(p2);
		
		// fabio altera o codigo;
		
		FiltroPessoa filtro01= new FiltroPorComprimento();
		
		System.out.println(filtro01.filter(listaPessoas));
		
		filtro01= new FiltroPorIdade();
		System.out.println(filtro01.filter(listaPessoas));
		
		filtro01= new FiltroPorSexo();
		System.out.println(filtro01.filter(listaPessoas));
		
	}
}
