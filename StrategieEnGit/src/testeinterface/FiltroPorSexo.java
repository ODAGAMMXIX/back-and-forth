package testeinterface;

import java.util.ArrayList;

public class FiltroPorSexo implements FiltroPessoa{

	@Override
	public ArrayList<Pessoa> filter(ArrayList<Pessoa> pessoas) {
		
		ArrayList<Pessoa> listaPessoasFiltrada = new ArrayList<Pessoa>();
		
		for (int i = 0; i < pessoas.size(); i++) {
			Pessoa p = pessoas.get(i);
			if (p.getSexo()=='f') {
				listaPessoasFiltrada.add(p);
			}
		}

		return listaPessoasFiltrada;
	}

}
