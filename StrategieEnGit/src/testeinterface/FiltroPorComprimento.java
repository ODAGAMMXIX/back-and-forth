package testeinterface;

import java.util.ArrayList;

public class FiltroPorComprimento implements FiltroPessoa{

	@Override
	public ArrayList<Pessoa> filter(ArrayList<Pessoa> pessoas) {
		
		ArrayList<Pessoa> listaPessoasFiltrada = new ArrayList<Pessoa>();
		
		for (int i = 0; i < pessoas.size(); i++) {
			Pessoa p = pessoas.get(i);
			if (p.getNome().length()>3) {
				listaPessoasFiltrada.add(p);
			}
		}

		return listaPessoasFiltrada;
	}

}
